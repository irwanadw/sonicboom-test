require("dotenv").config();

module.exports = {
  development: {
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: "postgres",
    define: {},
    dialectOptions: {
      options: {
        requestTimeout: 60000,
        statement_timeout: 100000,
        idle_in_transaction_session_timeout: 50000,
      },
    },
    pool: {
      max: 100,
      min: 0,
      acquire: 300000,
      idle: 100000,
    },
  },
};
