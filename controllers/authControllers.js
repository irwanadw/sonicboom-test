const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const db = require("../models");
class AuthControllers {
    static async register(req,res) {
        try {
            const salt = await bcrypt.genSalt(10)
            const hashPassword = await bcrypt.hash(req.body.password, salt)
            const inputAuth = {
                username: req.body.username,
                name: req.body.name,
                address:req.body.address,
                phone: req.body.phone,
                password: hashPassword,
                role: req.body.role || "customer"
            };
            const {password, ...others} = inputAuth
            if(!req.body.role || req.body.role === "") {
                res.status(400).json({
                    status: "Register Fail",
                    message: "You must fill your role"
                })
                if (req.body.role !== "customer") {
                    res.status(403).json({
                        message: "this route only for creating role customer"
                    })      
                }
            } else{
                if(req.body.role !== "customer") {
                    res.status(403).json({
                        status: "Register Fail",
                        message: "This route is for customer registration only! For employee registration, please contact your lead or director"
                    })
                } else {
                    await db.users.create(inputAuth)
                    res.status(201).json(others);
                }
            }

            
            
        } catch (error) {
            console.log(error)
            switch (error.errors[0].path) {
                case 'username':
                    res.status(400).json({
                        status: 'Register failed',
                        message: 'Your username has been registered'
                    });
                    break;
            
                default:
                    res.status(500).json(error);
                    break;
            }
        }
    }

    static async login(req,res) {
        try {
            const user = await db.users.findOne({
                where:{
                    username: req.body.username
                },
                raw:true
            })
            // console.log(user.id)
            !user && res.status(404).json({
                status: "login failed",
                message:`Users with username ${req.body.username} not registered`
            });
            const isPasswordMatch = await bcrypt.compare(req.body.password, user.password);
            
            const accesToken = jwt.sign({
                id: user.id,
                role:  user.role
            },
            process.env.SECRET_KEY,
            {expiresIn: "1d"}
            )
        
            const{ password, ...others} = user;
            // console.log(others)
            isPasswordMatch == true ? res.status(200).json({...others, accesToken}) : res.status(401).json({
                status: "login failed",
                message: "Wrong password"
            }); 
    
        } catch (error) {
            res.status(500).json(error)
        }
        
    }

    static async registerEmployee(req,res) {
        try {
            const salt = await bcrypt.genSalt(10)
            const hashPassword = await bcrypt.hash(req.body.password, salt)
            const inputAuth = {
                name: req.body.name,
                username: req.body.username,
                address:req.body.address,
                phone: req.body.phone,
                password: hashPassword,
                role: req.body.role
            };
            const {password, ...others} = inputAuth
            
            if(!req.body.role ||req.body.role === "") {
                res.status(400).json({
                    status: "Register Fail",
                    message: "You must fill your employee role"
                })
            }
            if(req.body.role === "customer") {
                res.status(403).json({
                    status: "Register Fail",
                    message: "This route is for employee registration only"
                })
            }
            switch (req.user.role) {
                case "lead":
                    req.body.role === "lead" || req.body.role === "director" ?
                    res.status(403).json({
                        Status: "Register Fail",
                        message: "You can only register your staff"
                    })
                    :
                    await db.users.create(inputAuth)
                    res.status(201).json({
                        Status: "Register Success",
                        data: others
                    })
                    break;

                case "staff":
                    req.body.role === "staff"
                    res.status(403).json({
                        Status: "Register Fail",
                        message: "You are not allowed to access this route! Only lead or director can access this route"
                    })
                    break;
            
                default:
                    await db.users.create(inputAuth)
                    res.status(201).json({
                        Status: "Register Success",
                        data:  others
                    })
                    break;
            }
        } catch (error) {
            switch (error.errors[0].path) {
                case 'username':
                    console.log("masuk sini");
                    res.status(400).json({
                        status: 'Register failed',
                        message: 'username has been registered'
                    });
                    break;
            
                default:
                    res.status(500).json({
                        status: 'Register failed',
                        message: error
                    });
                    break;
            }
        }
    }
}

module.exports = AuthControllers;