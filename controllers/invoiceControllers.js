const db = require("../models");

class InvoicesController {
    static async createInvoice(req,res) {
        try {
            // const idUser = req.params.userId
            // console.log(req.params.userId)
            const dataOrder = await db.orders.findAll({
                where: {
                    userID : req.params.userId,
                    isInvoice: false
                },
                raw:true
            })
            if(dataOrder.length === 0) {
                res.status(404).json({message: "Data Not Found"})
            } 

            const dataUser = await db.users.findOne({
                where:{
                    id: req.params.userId
                },
                raw:true
            })
         
            
            const inputInvoice = {
                userID: req.user.id,
                userName: dataUser.name,
                detailOrders: dataOrder,         
            }
            const a = await db.invoices.create(inputInvoice)
            console.log(a);
            
            

        } catch (error) {
            console.log(error);
        }
    }
}

module.exports = InvoicesController;