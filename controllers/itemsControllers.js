const db = require("../models");

class ItemsController {
    static async createItems(req,res) {
        try {  
            const inputItem = {
                name: req.body.name,
                price: req.body.price
            }
            const savedItem =  await db.items.create(inputItem);
            res.status(201).json({
                message: "added success",
                data: savedItem
            });
            
        } catch (error) {
            console.log("error", error)
            res.status(500).json(error);
        }
    }
}

module.exports = ItemsController