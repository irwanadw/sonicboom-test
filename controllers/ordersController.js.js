const db = require("../models");

class OrdersController {
    static async createOrder(req,res) {
        try {
            console.log(req.user, "INI USER");
            const itemSaved = await db.items.findOne({
                where:{
                    id: req.body.itemsID
                },
                raw:true
            })
            // const id =req.user.id
            const input = {
                itemsID: itemSaved.id,
                quantity: req.body.quantity,
                itemsPrice: itemSaved.price,
                userID: req.user.id,
                amount:  itemSaved.price*req.body.quantity,
                itemsName: itemSaved.name      
            }
            console.log(input, "INI INPUT")
            const updatedStock = itemSaved.stock-input.quantity
            await db.orders.create(input).then(
                await db.items.update({
                    stock: updatedStock
                },{where: {
                    id:itemSaved.id
                }})
            )
            res.status(201).json({
                status: "Added Success",
                data: input
            })
           
            
        } catch (error) {
            console.log(error);
        }
    }
}

module.exports = OrdersController;