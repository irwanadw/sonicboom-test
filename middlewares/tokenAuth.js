const jwt = require("jsonwebtoken");
// const {secretKey} = require("../controllers/auth");

const verifyToken = (req,res,next)=>{
    const authHeader = req.headers.token;
    if(authHeader) {
        const token = authHeader.split(" ")[1];
        jwt.verify(token,process.env.SECRET_KEY, (err,user)=>{
            if(err) {
                res.status(400).json({
                    status: "Authentication fail",
                    message: "token is not valid!"
                });
            } else {
                req.user = user;
                next();
            }
        });
    } else {
        return res.status(401).json({
            status: "Authentication fail",
            message:"You should login first!"
        })
    }
}

const verifyTokenAndAuthorization = (req,res,next) =>{
    verifyToken(req,res, ()=> {
        if(req.user.id === req.params.id ){
            next(); 
        } else {
            res.status(403).json("You are forbidden to do that, only Admin can do that!")
        }
    })

}

const verifyTokenAndEmployee = (req,res,next) =>{
    verifyToken(req,res, ()=> {
        if(req.user.role !== "customer"){
            next();
        } else {
            res.status(403).json("You are forbidden to do that, only employee can do that!")
        }
    })


}

const verifyStaffToken = (req,res,next) =>{
    verifyToken(req,res, ()=> {
        if(req.user.role === "staff"){
            next();
        
        } else {
            res.status(403).json("You are forbidden to do that, only staff can acces this route!")
        }
    })


}

// module.exports = verifyTokenAndAuthorization
module.exports = {
    verifyToken, 
    verifyTokenAndAuthorization,
    verifyTokenAndEmployee,
    verifyStaffToken
}