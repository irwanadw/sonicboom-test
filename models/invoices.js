'use strict';
const { TEXT } = require('sequelize');
const { STRING } = require('sequelize');
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Invoices extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Invoices.init({
    userID: DataTypes.STRING,
    userName: DataTypes.STRING,
    detailOrders: DataTypes.ARRAY(TEXT),
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    schema: "sonicboom",
    freezeTableName: true,
    sequelize,
    modelName: "invoices"
  });
  return Invoices;
};