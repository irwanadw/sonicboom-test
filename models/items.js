'use strict';
const {
  Model
} = require('sequelize');
const users = require('./users');
module.exports = (sequelize, DataTypes) => {
  class Items extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      
    }
  };
  Items.init({
    name: DataTypes.STRING,
    price: DataTypes.INTEGER,
    stock: DataTypes.INTEGER
    // createdAt: DataTypes.DATE,
    // updatedAt: DataTypes.DATE
  }, {
    schema: "sonicboom",
    freezeTableName: true,
    sequelize,
    modelName: "items"
  });
  return Items;
};