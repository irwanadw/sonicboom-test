'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Orders extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
     
    }
  };
  Orders.init({
    itemsID: DataTypes.STRING,
    quantity: DataTypes.INTEGER,
    itemsPrice: DataTypes.INTEGER,
    amount: DataTypes.INTEGER,
    userID: DataTypes.INTEGER,
    itemsName: DataTypes.STRING,
    isInvoice: DataTypes.BOOLEAN
  }, {
    schema: "sonicboom",
    freezeTableName: true,
    sequelize,
    modelName: "orders"
  });
  return Orders;
};