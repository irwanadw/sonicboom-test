const express = require("express");
const router = express.Router();
const {
    verifyToken, 
    verifyTokenAndEmployee
} = require("../middlewares/tokenAuth");
const authControllers = require("../controllers/authControllers");

router.post("/auth/register", authControllers.register);
router.post("/auth/login", authControllers.login);
router.post("/auth/register-employee", verifyTokenAndEmployee,authControllers.registerEmployee);
module.exports = router;