const express = require("express");
const router = express.Router();

const itemsRoute = require("./itemsRoute");
const ordersRoute = require("./ordersRoute");
const authRoute = require("./authRoute");
const invoicesRoute= require("./invoicesRoute");

router.use("/api", itemsRoute);
router.use("/api", ordersRoute);
router.use("/api", authRoute);
router.use("/api", invoicesRoute);

module.exports = router