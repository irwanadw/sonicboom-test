const express = require("express");
const InvoicesController = require("../controllers/invoiceControllers");
const router = express.Router();
const {
    verifyStaffToken, 
    verifyTokenAndEmployee
} = require("../middlewares/tokenAuth");

router.post("/invoices/:userId", verifyStaffToken,InvoicesController.createInvoice);

module.exports = router;
