const express = require("express");
const router = express.Router();
const {
    verifyToken, 
    verifyTokenAndEmployee
} = require("../middlewares/tokenAuth");
const itemsController = require("../controllers/itemsControllers");

router.post("/items",verifyTokenAndEmployee, itemsController.createItems);

module.exports = router;