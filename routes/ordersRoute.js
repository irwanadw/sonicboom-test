const express = require("express");
const OrdersController = require("../controllers/ordersController.js");
const router = express.Router();
const {
    verifyToken, 
    verifyTokenAndEmployee
} = require("../middlewares/tokenAuth");

router.post("/orders", verifyToken,OrdersController.createOrder);
module.exports = router;