require('dotenv').config();
const express = require("express");
const app = express();


const port = (process.env.PORT);
const indexRoute = require("./routes/indexRoutes");

app.use(express.json({limit: '50mb', extended: true}));
app.use(express.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000}))
app.use(indexRoute);

app.listen(port || 3000, () => {
    console.log(`server is listening http://localhost:${port}`);
});